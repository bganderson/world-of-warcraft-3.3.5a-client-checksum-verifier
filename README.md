# World of Warcraft 3.3.5a Client Checksum Verifier

Verifies 3.3.5a client md5 or sha1 checksums

# Requirements 

Python > 3.9

# Usage

Drop into your root client directory with `Wow.exe` and run with `python wotlk_checksum_verifier.py`

If you want to run from a different directory, you **must** specify the checksum **and** the client path `python wotlk_checksum_verifier.py sha1|md5 [path]`

It will default to `sha1` and the current directory if you don't specify any options

# License

Released under the BSD 2-Clause License